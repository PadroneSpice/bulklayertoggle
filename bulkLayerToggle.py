# Bulk Layer Toggle Script by Sean Castillo

from krita import *

doc  = Krita.instance().activeDocument()
if not doc is None:
    root = doc.rootNode()

    layerToToggle = "Colors"

    def recursion(layer):
        if layer.name() == layerToToggle:
            layer.setVisible(not layer.visible())
        if layer.type() == "grouplayer":
            for i in layer.childNodes():
                recursion(i)

    for i in root.childNodes():
        recursion(i)
    doc.refreshProjection()
