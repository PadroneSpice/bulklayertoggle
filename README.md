# README #

Bulk Layer Toggle by Sean Castillo
1 May 2020

Krita Python Script to toggle the visibility of all layers in the document that have the specified name.
This could be altered to search by a criterion other than name (such as blending mode or color label),
or to do something other than toggle visibility (such as change opacity).